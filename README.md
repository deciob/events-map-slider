### How to run: ###


```
#!bash

git clone git@bitbucket.org:deciob/events-map-slider.git

cd events-map-slider

python -m SimpleHTTPServer
```

### Relevant files: ###

* index.html
* js/main.js