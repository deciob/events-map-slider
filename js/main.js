// Using this simple Pub/Sub implementation to communicate between the slider and the map.
// If this is going to be used in the WordPress application it shoul be loaded as a
// separate file, after jQuery and before the slider and the google maps code.

/* jQuery Tiny Pub/Sub - v0.7 - 10/27/2011
 * http://benalman.com/
 * Copyright (c) 2011 "Cowboy" Ben Alman; Licensed MIT, GPL */

(function($) {

  var o = $({});

  $.subscribe = function() {
    o.on.apply(o, arguments);
  };

  $.unsubscribe = function() {
    o.off.apply(o, arguments);
  };

  $.publish = function() {
    o.trigger.apply(o, arguments);
  };

}(jQuery));


function onGoogleMapsReady () {

  //**************************************************************************************
  // Data that should come from one or more api calls:

  // sample response with same data structure returned from
  // admin-ajax.php
  var apiAdminAjax = {
    html: "Placeholder",
    markers: [
      {
        lat: "49.35",
        lng: "11",
        title: "Straight Bourbon - Tour",
        address: "Friedrichstra\u00dfe 12",
        link: "http:\/\/sittu.de\/event\/straight-bourbon-tour\/",
        venue_id: "27",
        event_id: 25,
        // This is not part of the admin-ajax.php response,
        // but it is needed for the interaction with the slider.
        date: 1455363569423 - 1000 * 60 * 60 * 6
      },{
        lat: "49.39",
        lng: "11.05",
        title: "Bundesligaspiel: Bayern M\u00fcnchen - VfL Wolfsburg",
        address: "Werner-Heisenberg-Allee 25",
        link: "http:\/\/sittu.de\/event\/bundesligaspiel-bayern-munchen-vfl-wolfsburg\/",
        venue_id: "29",
        event_id: 28,
        // This is not part of the admin-ajax.php response,
        // but it is needed for the interaction with the slider.
        date: 1455363569423 - 1000 * 60 * 60 * 3
      },
      {
        lat: "49.45",
        lng: "11.08",
        title: "Straight Bourbon - Tour",
        address: "Friedrichstra\u00dfe 12",
        link: "http:\/\/sittu.de\/event\/straight-bourbon-tour\/",
        venue_id: "27",
        event_id: 29,
        // This is not part of the admin-ajax.php response,
        // but it is needed for the interaction with the slider.
        date: 1455363569423 - 1000 * 60 * 60 * 2
      },{
        lat: "49.49",
        lng: "11.21",
        title: "Bundesligaspiel: Bayern M\u00fcnchen - VfL Wolfsburg",
        address: "Werner-Heisenberg-Allee 25",
        link: "http:\/\/sittu.de\/event\/bundesligaspiel-bayern-munchen-vfl-wolfsburg\/",
        venue_id: "29",
        event_id: 30,
        // This is not part of the admin-ajax.php response,
        // but it is needed for the interaction with the slider.
        date: 1455363569423 - 1000 * 60 * 60 * 2
      }
    ],
    "success":true,
    "tribe_paged":"1",
    "max_pages":1,
    "total_count":"2",
    "view":"past"
  };

  // The following could be included into the admin-ajax.php response

  // This could also be calculated on the client side,
  // taking into account all the dates in apiAdminAjax.markers
  var apiEvtBounds = {
    min: 1455363569423 - 1000 * 60 * 60 * 6 /*6 hours back*/,
    max: 1455363569423
  };

  // This could be dinamic, depending on the apiEvtBounds
  var apiEvtRange = {
    min: {hours: 1},
    max: {hours: 5}
  };

  // This could be dinamic, depending on the apiEvtBounds
  var apiMapOptions = {
    center: {lat: 49.44, lng: 11.08},
    zoom: 10,
  }
  //**************************************************************************************

  // Data transformations

  Object.keys(apiEvtBounds).forEach(function(k) {
    apiEvtBounds[k] = new Date(apiEvtBounds[k]);
  });


  // Slider

  var $slider = $("#slider");

  var formatter = function(val){
    var hours = ('0' + val.getHours()).slice(-2);
    var minutes = ('0' + val.getMinutes()).slice(-2);
    return hours + ":" + minutes;
  };

  $slider.dateRangeSlider({
    bounds: apiEvtBounds,
    formatter: formatter,
    range: apiEvtRange,
  });

  $slider.bind("valuesChanged", function(e, data) {
    // Using a simple JQuery pub/sub to communicate between the slider and the map.
    $.publish("dateBounds", data.values);
  });


  // Map
  // The WordPress application seems to have its own google maps plugin implementation,
  // so this is only example code and for the real implementation one will need to hook
  // into the relevant WP plugin.

  var markers = {};
  var map = new google.maps.Map(document.getElementById('map'), {
    center: apiMapOptions.center,
    zoom: apiMapOptions.zoom,
  });

  var setMarkers = function(e, dateBounds) {
    var min = +dateBounds.min;
    var max = +dateBounds.max;

    apiAdminAjax.markers.forEach(function(d) {
      var marker;
      var latlng;

      if (markers[d.event_id]) {
        marker = markers[d.event_id];
      } else {
        latlng = new google.maps.LatLng(+d.lat, +d.lng);
        marker = new google.maps.Marker({
          position: latlng,
          title: d.title
        });
        markers[d.event_id] = marker;
      }

      if (d.date >= min && d.date <= max) {
        // Show markers if in date range
        marker.setMap(map);
      } else {
        // If not hide
        marker.setMap(null);
      }
    });
  };

  // Listen to the slider events
  $.subscribe("dateBounds", setMarkers);
}
